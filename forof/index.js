/*
 * @Author: FBB
 * @Date: 2020-06-04 10:33:34
 * @LastEditors: FBB
 * @LastEditTime: 2020-06-06 17:24:30
 * @Description:
 */
function createIterator(items) {
  var i = 0;
  function addIterator(items) {
    return {
      next() {
        return i >= items.length
          ? { done: true, vlaue: undefined }
          : { done: false, value: items[i++] };
      },
    };
  }
  var iterator = addIterator(items);
  iterator[Symbol.iterator] = function () {
    return addIterator(items);
  };
  return iterator;
}

function ForOf(obj, cb) {
  var result, iterable;
  if (typeof obj[Symbol.iterator] !== "function") {
    throw new TypeError("not iterable");
  }
  if (typeof cb !== "function") {
    throw new TypeError("cb must be callable");
  }
  iterable = obj[Symbol.iterator]();
  result = iterable.next();
  while (!result.done) {
    cb(result.value);
    result = iterable.next();
  }
}

const obj = {
  name: "FBB",
  age: 23,
  major: "software",
};

obj[Symbol.iterator] = function () {
  return createIterator([1, 2, 3, 4]);
};

ForOf(obj, console.log);
